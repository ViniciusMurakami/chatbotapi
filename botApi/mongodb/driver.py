from flask_mongoengine import MongoEngine

chatbot_db = MongoEngine()

def load_mongo(app):
    chatbot_db.init_app(app)