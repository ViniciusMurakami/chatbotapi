from mongodb.driver import chatbot_db

class Message(chatbot_db.Document):
    id_message = chatbot_db.StringField(required=False, unique=False)
    conversationId = chatbot_db.StringField(required=True, unique=False)
    timestamp = chatbot_db.StringField(required=False, unique=False)
    from_ = chatbot_db.StringField(required=True, unique=False)
    to = chatbot_db.StringField(required=True, unique=False)
    text = chatbot_db.StringField(required=True, unique=False)
     
class Bot(chatbot_db.Document):
    bot_id = chatbot_db.StringField(required=True, unique=True)
    name = chatbot_db.StringField(required=True, unique=False)