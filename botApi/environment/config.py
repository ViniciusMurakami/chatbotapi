class Config(object):
    DEBUG = False
    TESTING = False
    MONGODB_SETTINGS = {'host': 'mongodb://localhost:27017/chatbotapi'}

class DockerEnv(Config):
    DEBUG = True