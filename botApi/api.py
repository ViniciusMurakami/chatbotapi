from flask import Flask
from flask_restful import Api
from route import routes 
from mongodb.driver import load_mongo
from resources.error import errors
from environment.config import DockerEnv 
app = Flask(__name__)

#Set the environment ENV_FILE pointing to .env file
app.config.from_object(DockerEnv())
api = Api(app, errors = errors)

load_mongo(app)
routes(api)

app.run()