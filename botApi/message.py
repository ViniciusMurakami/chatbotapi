from flask import   Response, request
from flask_restful import Resource

from mongoengine.errors import FieldDoesNotExist, ValidationError, InvalidQueryError
from resources.error import SchemaValidationError, InternalServerError

from mongodb.model import Message, Bot

from datetime import datetime 
import uuid

class MessagesApi(Resource):
    def get(self):
        messages = Message.objects().exclude('id').to_json().replace('from_','from').replace('id_message','id')
        return Response(messages, mimetype="application/json", status=200)
    
    def post(self):
        try:
            payload = request.get_json()
            if payload.get('timestamp') is None:
                timestamp = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")
            else:
                timestamp = payload.get('timestamp')
            
            #If the message comes from Bot to User, then ID = botId 
            who_am_i = payload.get('to')
            if Bot.objects(bot_id=who_am_i) is None:
                id_message = payload.get('to')
      
            #Otherwise, generate a new ID
            else:
                id_message = str(uuid.uuid4())


            messages = Message(id_message=id_message,
                                 conversationId=payload.get('conversationId'),
                                 timestamp=timestamp,
                                 from_=payload.get('from'), 
                                 to=payload.get('to'),
                                 text=payload.get('text'))
            messages.save()
            return 'OK', 200
        except (FieldDoesNotExist, ValidationError):
            raise SchemaValidationError
        except Exception as e:
            raise InternalServerError

class MessageApi(Resource):
    def get(self, id):
        try:
            messages = Message.objects.exclude('id').get(id_message=id).to_json().replace('from_','from').replace('id_message','id')
            return Response(messages, mimetype="application/json", status=200)
        except Exception:
            raise InternalServerError

class ConversationApi(Resource):
    def get(self, conversationId):
        try:
            conversation = Message.objects.exclude('id').get(conversationId=conversationId).to_json().replace('from_','from').replace('id_message','id')
            return Response(conversation, mimetype="application/json", status=200)
        except Exception:
            raise InternalServerError