from message import MessagesApi, MessageApi, ConversationApi
from bot import BotsApi, BotApi

def routes(api):
    api.add_resource(BotsApi, '/api/bots', '/api/bots/')
    api.add_resource(BotApi, '/api/bots/<id>')

    api.add_resource(MessagesApi, '/api/messages', '/api/messages/')
    api.add_resource(MessageApi, '/api/messages/<id>')
    api.add_resource(ConversationApi, '/api/messages?conversationId=<conversationId>')