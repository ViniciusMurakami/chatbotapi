from flask import   Response, request
from flask_restful import Resource

from mongoengine.errors import FieldDoesNotExist,  ValidationError, InvalidQueryError
from resources.error import SchemaValidationError, InternalServerError

from mongodb.model import Bot

class BotsApi(Resource):
    def get(self):
        bots = Bot.objects().exclude('id').to_json().replace('bot_id','id')
        return Response(bots, mimetype="application/json", status=200)
    
    def post(self):
        try:
            payload = request.get_json()
            bot = Bot(name=payload.get('name'), bot_id=payload.get('id'))
            bot.save()
            id = bot.bot_id
            return {'id':str(id)}, 200
        except (FieldDoesNotExist, ValidationError):
            raise SchemaValidationError
        except Exception as e:
            raise InternalServerError

class BotApi(Resource):
    def put(self, id):
        try:
            payload = request.get_json()
            Bot.objects.get(bot_id=id).update(name=payload.get('name'), bot_id=payload.get('id'))
            return 'OK', 200
        except InvalidQueryError:
            raise SchemaValidationError
        except Exception:
            raise InternalServerError
        
    def delete(self, id):
        try:
            bot = Bot.objects.get(bot_id=id)
            bot.delete()
            return 'OK', 200
        except Exception:
            raise InternalServerError
    
    def get(self, id):
        try:
            bot = Bot.objects().exclude('id').get(bot_id=id).to_json().replace('bot_id','id')
            return Response(bot, mimetype="application/json", status=200)
        except Exception:
            raise InternalServerError