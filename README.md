# Chat Bot Api Challenge
Welcome to my gitlab folks! 
I'm Vinicius Murakami, have been working as data architect for the past years.
If you're looking for more details, please step in my profile below:
https://www.linkedin.com/in/vinicius-higa-murakami-26069388/

##  Solution / Tools
For the challenge I've chosen Python with Flask + MongoDB.

##  Pre-Reqs
 - [ ] [Docker]([https://www.docker.com/products/docker-desktop](https://www.docker.com/products/docker-desktop))
 - [ ] [Python 3.x]([https://www.python.org/downloads/](https://www.python.org/downloads/))
 - [ ] [Flask]([https://palletsprojects.com/p/flask/](https://palletsprojects.com/p/flask/))
 - [ ] [MongoDB]([https://www.mongodb.com/](https://www.mongodb.com/))
 - [ ] [PostMan]([https://www.postman.com/downloads/](https://www.postman.com/downloads/))
 - [ ] [MongoDB Compass]([https://www.mongodb.com/products/compass](https://www.mongodb.com/products/compass))

## Setup MongoDB
Get your Docker up and download the following image:

      docker pull mongo
Now get the MongoDB up and running:

      docker run -d -p 27017:27017 -p 28017:28017 -e MONGODB_PASS="changeme" mongo

>Details on: [https://hub.docker.com/_/mongo?tab=description](https://hub.docker.com/_/mongo?tab=description)
##  Setup Python
First of all, install **pipenv**.

    pip install --user pipenv

If you're running on Windows 

    set PATH=%PATH%;set PATH=%PATH%;'C:\Users\<Username>\AppData\Roaming\Python\<PythonLatestVersion>\Scripts'
Restart your IDE or Terminal, then you should be able to run the following cmd

    pipenv install -r chatbotapi/requirements.txt
##  Execute the Project
Login into a terminal or through IDE and start a virtualenv
    
    pipenv shell
Start the bot Api

    python chatbotapi/botApi/api.py

## Important infos
Since we're running on local, we should have the following:
>Address, Port and DB

|                |API|MongoDB                         |
|----------------|-------------------------------|-----------------------------|
|Address|localhost:5000|mongodb://localhost:27017
|Collection|--            |chatbotapi            |
-------------------------
>Payload BOT Requirements 

|Field|Type|Optional|Default Value
|------|-----|-------------|-------|
|id|String|No|-
|name|String|No|-

>API Bot Routes 

|Method|Path |Description 
|------|-----|-------------|
|GET|/api/bots|List all bots
|GET|/api/bots/id|Search for an specific bot
|POST|/api/bots|Insert a new bot
|PUT|/api/bots/id|Update an existent bot
|DELETE|/api/bots/id|Delete an existent bot
---------------

>Payload MESSAGE Requirements 

|Field|Type|Optional|Default Value
|------|-----|-------------|-------|
|conversationId|String|No|-
|timestamp|timestamp|Yes|now()
|from|String|No|-
|to|String|No|-
|text|String|No|-
>API Messages Routes

|Method|Path |Description 
|------|-----|-------------|
|GET|/api/messages|List all messages/conversations
|GET|/api/messages/id|Retrive an specific message
|GET|/api/messages?conversationId=id|Return messages regarding a conversation
|POST|/api/messages|Insert a new message from User or Bot




## Api Calls - BOT
Open your PostMan (or SoapUI, curl or whatever tool). 
And make a quick test on Bot endpoint

**POST localhost:5000/api/bots**

	{
		"id": "36b9f842-ee97-11e8-9443-0242ac120002",
		"name": "Aureo"
	}
	
	###################################################
	## Should return
	###################################################
	{
		"id":  "36b9f842-ee97-11e8-9443-0242ac120002"
	}
**GET localhost:5000/bots/36b9f842-ee97-11e8-9443-0242ac120002**

	{
		"id": "36b9f842-ee97-11e8-9443-0242ac120002",
		"name": "Aureo"
	}
**GET localhost:5000/api/bots**
	
	{
		"id": "36b9f842-ee97-11e8-9443-0242ac120002",
		"name": "Aureo"
	}

**PUT localhost:5000/api/bots/36b9f842-ee97-11e8-9443-0242ac120002**

	{
	    "id": "36b9f842-ee97-11e8-9443-0242ac120002",
	    "name": "Telefonica/Vivo"
	}
	
	###################################################
	## Should return
	###################################################
	"OK"

**DELETE localhost:5000/api/bots/36b9f842-ee97-11e8-9443-0242ac120002**

	###################################################
	## Should return
	###################################################
	"OK"

## Api Calls - MESSAGE
Open your PostMan (or SoapUI, curl or whatever tool). 
And make a quick test on Message endpoint

**POST localhost:5000/api/messages**

	{
		"conversationId": "7665ada8-3448-4acd-a1b7-d688e68fe9a1",
		"from": "36b9f842-ee97-11e8-9443-0242ac120002",
		"to": "16edd3b3-3f75-40df-af07-2a3813a79ce9",
		"text": "Oi! Como posso te ajudar?"
	},
	{
	    "conversationId": "7665ada8-3448-4acd-a1b7-d688e68fe9a1",
	    "from": "16edd3b3-3f75-40df-af07-2a3813a79ce9",
	    "to": "36b9f842-ee97-11e8-9443-0242ac120002",
	    "text": "Gostaria de saber meu saldo?"
	}
	
	###################################################
	## Should return
	###################################################
	"OK"
	
**GET localhost:5000/messages/**

	{
		"id":  "f3c6cb2d-107e-4679-9ea9-57c787bde122",
		"conversationId":  "7665ada8-3448-4acd-a1b7-d688e68fe9a1",
		"timestamp":  "2020-05-03T20:57:52.913532Z",
		"from":  "36b9f842-ee97-11e8-9443-0242ac120002",
		"to":  "16edd3b3-3f75-40df-af07-2a3813a79ce9",
		"text":  "Oi! Como posso te ajudar?"
		},
	{
		"id":  "90f0b94c-5484-4ecf-b849-e81d7bcc54ac",
		"conversationId":  "7665ada8-3448-4acd-a1b7-d688e68fe9a1",
		"timestamp":  "2020-05-03T20:57:59.075826Z",
		"from":  "16edd3b3-3f75-40df-af07-2a3813a79ce9",
		"to":  "36b9f842-ee97-11e8-9443-0242ac120002",
		"text":  "Gostaria de saber meu saldo?"
	}

**GET localhost:5000/api/messages/f3c6cb2d-107e-4679-9ea9-57c787bde122**
	
	{
		"id":  "f3c6cb2d-107e-4679-9ea9-57c787bde122",
		"conversationId":  "7665ada8-3448-4acd-a1b7-d688e68fe9a1",
		"timestamp":  "2020-05-03T20:57:52.913532Z",
		"from":  "36b9f842-ee97-11e8-9443-0242ac120002",
		"to":  "16edd3b3-3f75-40df-af07-2a3813a79ce9",
		"text":  "Oi! Como posso te ajudar?"
	}
**GET localhost:5000/api/messages?conversationId=7665ada8-3448-4acd-a1b7-d688e68fe9a1**

	{
		"id":  "f3c6cb2d-107e-4679-9ea9-57c787bde122",
		"conversationId":  "7665ada8-3448-4acd-a1b7-d688e68fe9a1",
		"timestamp":  "2020-05-03T20:57:52.913532Z",
		"from":  "36b9f842-ee97-11e8-9443-0242ac120002",
		"to":  "16edd3b3-3f75-40df-af07-2a3813a79ce9",
		"text":  "Oi! Como posso te ajudar?"
		},
	{
		"id":  "90f0b94c-5484-4ecf-b849-e81d7bcc54ac",
		"conversationId":  "7665ada8-3448-4acd-a1b7-d688e68fe9a1",
		"timestamp":  "2020-05-03T20:57:59.075826Z",
		"from":  "16edd3b3-3f75-40df-af07-2a3813a79ce9",
		"to":  "36b9f842-ee97-11e8-9443-0242ac120002",
		"text":  "Gostaria de saber meu saldo?"
	}

## TODO LIST
 - [ ] Implement unit tests
 - [ ] Relationship on bot x message (deletion)
 - [ ] Improve error.py messages
 - [ ] Authorization | Authentication (token, login)
 - [ ] Log Trail
 - [ ] Docker File
 - [ ] Search properties in URL
 - [ ] Test drive of RethinkDB


## UML diagrams

And lastly I tried to build an image to represent the interaction in my py modules.

```mermaid
classDiagram
api --|> config
route --|> api
api --|> error
api <|-- driver
driver <|-- model
bot <|-- api
message <|-- api
api : run()
api : config()
api : driver MongoDB
api : api Flask_RestFul
api : routes Flask_RestFul
api : error()
bot : Object[] BotsApi
bot : Object[] BotApi
bot : Resources Flask_RestFul
bot : get()
bot : post()
bot : put()
bot : delete()
config : Object[] DockerEnv
driver : chatbot_db MongoEngine
driver : load_mongo()  
error : Object[] InternalServerError
error : Object[] SchemaValidationError
message : Object[] MessagesApi
message : Object[] MessageApi
message : Object[] ConversationApi
message : Resources Flask_RestFul
message : get()
message : post()
model : Object[] Message
model : Object[] Bot
route : routes()
route : add_resource()
```
-------------------------
